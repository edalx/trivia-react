import React, {Component} from 'react';
import './components/static/css/App.css';
import QuestionList from './components/trivia/QuestionList';
import BoxScore from './components/trivia/BoxScore';
import Results from './components/trivia/Results';
import logo from './components/static/images/logo.png';


class App extends Component {
    constructor() {
        super();
        this.state = {
            questions: [],
            score: 0,
            current: 1
        };
    }

    componentWillMount() {
        fetch('https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean')
            .then(response => response.json())
            .then(questions => {
                let count = 1;
                let rq = [];
                questions.results.forEach(qt => {
                    let c_a = 'a';
                    if (qt.correct_answer === 'False') {
                        c_a = 'b';
                    }
                    let data = {
                        id: count++,
                        category: qt.category,
                        difficulty: qt.difficulty,
                        text: qt.question,
                        correct: c_a,
                        choices: [
                            {
                                id: 'a',
                                text: 'Verdadero',
                            },
                            {
                                id: 'b',
                                text: 'Falso',
                            }
                        ],
                        productImageUrl: 'public/static/image-yellow.png'
                    };
                    rq.push(data);
                });
                this.setState({
                    questions: rq,
                })
            })
    }


    setCurrent(current) {
        this.setState({current});
    }

    setScore(score) {
        this.setState({score});
    }


    render() {
        var boxscore = '';
        var results = '';
        if (this.state.current > this.state.questions.length) {
            boxscore = '';
            results = <Results {...this.state}/>
        } else {
            boxscore = <BoxScore {...this.state} />
            results = '';
        }

        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                </header>
                <div>
                    {boxscore}
                    <QuestionList setScore={this.setScore.bind(this)}
                                  setCurrent={this.setCurrent.bind(this)} {...this.state}/>
                    {results}
                </div>
            </div>
        );
    }
}

export default App;
