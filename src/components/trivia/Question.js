import React, {Component} from 'react';
import '../static/css/App.css'
import qt from '../static/images/question.png';


class Question extends Component {
    handleChange(e) {
        const {setCurrent, setScore, question} = this.props;
        e.preventDefault();
        const selected = e.target.value;
        setCurrent(this.props.current + 1);
        if (selected === question.correct) {
            setScore(this.props.score + 1);
        }
    }

    render() {
        const {question} = this.props;
        return (
            <div>
                <div>
                    <br/>
                    <img src={qt} className="App-img-question" alt="logo"/>
                    <div className="Description-question">
                        <div className="Category">
                            <span>Category: </span>
                            <span><strong>{question.category}</strong></span>
                        </div>
                        <div className="Question-text">
                            <h3 dangerouslySetInnerHTML={{__html: question.text}} className="App-question"></h3>
                        </div>
                        <div className="Difficulty">
                            <span>Difficulty: </span>
                            <span><strong>{question.difficulty}</strong></span>
                        </div>
                    </div>
                    <br/>
                    <br/>
                </div>
                <div>
                    <hr/>
                    <br/>
                    <div className="Choices">
                        <div className="Header-options">
                            <h3>Opciones: </h3>
                        </div>
                        <ul className="list-group">
                            {
                                question.choices.map(choice => {
                                    return (
                                        <li className="list-group-item" key={choice.id}>{choice.id}
                                            <input onChange={this.handleChange.bind(this)} type="radio"
                                                   name={question.id}
                                                   value={choice.id}/> {choice.text}
                                        </li>
                                    );
                                })
                            }
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default Question;