import React from 'react';

class BoxScore extends React.Component {
    render() {
        return (
            <div>
                <strong>Pregunta {this.props.current} de {this.props.questions.length}</strong>
            </div>
        );
    }
}

export default BoxScore;
