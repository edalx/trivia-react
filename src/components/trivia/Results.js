import React from 'react';

class Results extends React.Component {

    render() {
        var percent = (this.props.score / this.props.questions.length * 100);
        var message = ''
        if (percent > 80) {
            message = 'Excelente!';
        } else if (percent < 80 && percent > 60) {
            message = 'Nada mal!';
        } else {
            message = 'Sigue participando';
        }
        return (
            <div>
                <h4>Acertaste {this.props.score} de {this.props.questions.length}</h4>
                <h2>{percent}% - {message}</h2>
                <a className="btn btn-primary" href="http://localhost:3000">Intentalo nuevamente</a>

            </div>
        );
    }
}

export default Results;
